package main

import (
	"bufio"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/RedDocMD/blocked/bbl"
)

func main() {
	args := os.Args
	if len(args) != 3 {
		log.Fatalf("Expected: `%s: <infile> <outfile>`\n", args[0])
	}

	fileName := args[1]
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	quads := []bbl.Quad{}

	for idx := uint(1); scanner.Scan(); idx++ {
		line := strings.TrimSpace(scanner.Text())
		var newQuad bbl.Quad
		if line == "S" {
			newQuad = bbl.NewQuad(idx, bbl.Simple, nil)
		} else if strings.HasPrefix(line, "U") {
			spcIdx := strings.Index(line, " ")
			num, err := strconv.Atoi(line[spcIdx+1:])
			if err != nil {
				log.Fatalln(err)
			}
			unum := uint(num)
			newQuad = bbl.NewQuad(idx, bbl.UncondJump, &unum)
		} else if strings.HasPrefix(line, "C") {
			spcIdx := strings.Index(line, " ")
			num, err := strconv.Atoi(line[spcIdx+1:])
			if err != nil {
				log.Fatalln(err)
			}
			unum := uint(num)
			newQuad = bbl.NewQuad(idx, bbl.CondJump, &unum)
		} else {
			log.Fatalf("Unknown quad type: %s\n", line)
		}
		quads = append(quads, newQuad)
	}

	blocks := bbl.NewCFG(quads)

	outFileName := args[2]
	outFile, err := os.Create(outFileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer outFile.Close()

	bbl.WriteGraph(blocks, outFile)

	pdfFileName := strings.ReplaceAll(outFileName, ".dot", ".pdf")
	// In case arg[2] doesn't end with .dot
	if !strings.Contains(pdfFileName, ".pdf") {
		pdfFileName += ".pdf"
	}
	cmd := exec.Command("/usr/bin/dot", 
		"-Tpdf", "-o", pdfFileName, outFileName)
	err = cmd.Run()
	if err != nil {
		log.Fatalln(err)
	}
}
