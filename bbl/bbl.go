package bbl

type BasicBlock struct {
	idx       uint
	quads     []Quad
	preds     []*BasicBlock
	succs     []*BasicBlock
	leaderIdx uint
}

func NewCFG(quads []Quad) []*BasicBlock {
	quadGrps := partitionQuads(quads)
	blocks := baseBlocks(quadGrps)

	leaderMap := blockByLeaderIdx(blocks)
	// Assign successors
	for idx, block := range blocks {
		if idx == len(blocks)-1 {
			break
		}
		lastQuad := block.quads[len(block.quads)-1]
		switch lastQuad.kind {
		case Simple:
			block.succs = append(block.succs, blocks[idx+1])
		case UncondJump:
			block.succs = append(
				block.succs,
				leaderMap[*lastQuad.target],
			)
		case CondJump:
			block.succs = append(
				block.succs,
				blocks[idx+1],
				leaderMap[*lastQuad.target],
			)
		}
	}

	// Assign predecessors
	for _, block := range blocks {
		for _, succ := range block.succs {
			succ.preds = append(succ.preds, block)
		}
	}

	return blocks
}

func blockByLeaderIdx(blocks []*BasicBlock) map[uint]*BasicBlock {
	leaderMap := make(map[uint]*BasicBlock)
	for _, block := range blocks {
		leaderMap[block.leaderIdx] = block
	}
	return leaderMap
}

func baseBlocks(quadGrps [][]Quad) []*BasicBlock {
	var blocks []*BasicBlock
	for idx, quads := range quadGrps {
		block := &BasicBlock{
			idx:       uint(idx) + 1,
			quads:     quads,
			leaderIdx: quads[0].idx,
		}
		blocks = append(blocks, block)
	}
	return blocks
}

func partitionQuads(quads []Quad) [][]Quad {
	isTarget := make([]bool, len(quads)+1)

	// Mark statements that are targets of jumps
	for _, quad := range quads {
		if quad.kind == UncondJump || quad.kind == CondJump {
			isTarget[*quad.target] = true
		}
	}

	var quadGrps [][]Quad
	var currGrp []Quad

	for _, quad := range quads {
		if isTarget[quad.idx] {
			quadGrps = append(quadGrps, currGrp)
			currGrp = nil
		}
		currGrp = append(currGrp, quad)
		if quad.kind == CondJump {
			quadGrps = append(quadGrps, currGrp)
			currGrp = nil
		}
	}
	if len(currGrp) != 0 {
		quadGrps = append(quadGrps, currGrp)
	}

	return quadGrps
}
