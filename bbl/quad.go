package bbl

import "fmt"

type QuadType = int

const (
	Simple QuadType = iota
	CondJump
	UncondJump
)

func quadTypeStr(k QuadType) string {
	var str string
	switch k {
	case Simple:
		str = "Simple"
	case CondJump:
		str = "Conditional Jump"
	case UncondJump:
		str = "Unconditional Jump"
	}
	return str
}

type Quad struct {
	idx    uint
	kind   QuadType
	target *uint
}

func NewQuad(idx uint, kind QuadType, target *uint) Quad {
	return Quad{idx, kind, target}
}

func (q Quad) String() string {
	s := fmt.Sprintf("%d: %s", q.idx, quadTypeStr(q.kind))
	if q.target != nil {
		s += fmt.Sprintf(" => %d", *q.target)
	}
	return s
}
