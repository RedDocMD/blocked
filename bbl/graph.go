package bbl

import (
	"fmt"
	"io"
)

func (bbl *BasicBlock) label() string {
	str := ""
	for idx, quad := range bbl.quads {
		str += fmt.Sprint(quad.idx)
		if idx != len(bbl.quads)-1 {
			str += "\\n"
		}
	}
	return str
}

func WriteGraph(blocks []*BasicBlock, out io.Writer) error {
	fmt.Fprintln(out, "digraph G {")
	// Define edges
	for _, block := range blocks {
		for _, succ := range block.succs {
			fmt.Fprintf(out,
				"    b%d -> b%d;\n", block.idx, succ.idx)
		}
	}
	// Define nodes
	for _, block := range blocks {
		fmt.Fprintf(out, "    b%d [label=\"%s\", shape=box];\n",
			block.idx, block.label())
	}
	fmt.Fprintln(out, "}")
	return nil
}
